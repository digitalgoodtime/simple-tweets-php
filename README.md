################################################################
Simple Tweets - Simple Twitter User Timeline Version 1.1 Parser
################################################################

Introduction
------------

Simple Tweet provides a simple way of retrieving the user timeline by the screen name
of the twitter account.  It will also provide the ability to cache off those results in
a JSON file for the requested amount of time.  This will help with Twitter's rate limiting.

I looked all over the internet for a way to retrieve a user timeline in PHP and have the
ability to cache it off without using CURL and a caching plugin, but couldn't find anything.
After Frankensteining bits of code from multiple projects, I came up with this simple, yet
effective bit of logic that just works.

Twitter's documentation about the user timeline API can be found here:
https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline

Examples
--------

See index.php file.


Contact
-------

The author is Adam Norris <adam.norris@digitalgoodtime.net>. The project resides at
http://bitbucket.org/digitalgoodtime/simple-tweets-php. If you find bugs, or have feature
requests, please report them in the project site issue tracker. Patches are
also very welcome.

Contributors
------------

- Adam Norris
- Bits of the Internet

