<?php

// Number of seconds a page should remain cached for
$cache_expires = 120;

// Path to the cache folder
$cache_folder = "cache/";

function getTweets($key, $secret, $username, $count)
{
    $data;
    $cache_file = $username . ".json";
    
    if (is_cached($cache_file)) {
        $data = read_cache($cache_file);
    } else {
        
        $authContext = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => "Authorization: Basic " . base64_encode(($key) . ':' . ($secret)) . "\r\n" . "Content-type: application/x-www-form-urlencoded;charset=UTF-8\r\n" . "Content-Length: 29\r\n" . "\r\n" . "grant_type=client_credentials"
            )
        ));
        
        $authResponse = file_get_contents("https://api.twitter.com/oauth2/token", false, $authContext);
        $decodedAuth  = json_decode($authResponse, true);
        $bearerToken  = $decodedAuth["access_token"];
        
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => "Authorization: Bearer " . $bearerToken . "\r\n" . "\r\n" . "grant_type=client_credentials"
            )
        ));
        
        $data = file_get_contents('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' . $username . '&count=' . $count, false, $context);
        
        write_cache($cache_file, $data);
        
    }
    
    return json_decode($data, true);
}

// Checks whether the page has been cached or not
function is_cached($file)
{
    global $cache_folder, $cache_expires;
    $cachefile         = $cache_folder . $file;
    $cachefile_created = (file_exists($cachefile)) ? @filemtime($cachefile) : 0;
    return ((time() - $cache_expires) < $cachefile_created);
}

// Reads from a cached file
function read_cache($file)
{
    global $cache_folder;
    $cachefile = $cache_folder . $file;
    return file_get_contents($cachefile);
}

// Writes to a cached file
function write_cache($file, $out)
{
    global $cache_folder;
    $cachefile = $cache_folder . $file;
    $fp        = fopen($cachefile, 'w');
    fwrite($fp, $out);
    fclose($fp);
}

?>